/**
* Schemas.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName:'tbl_schemas',
  attributes: {
  	id:{type:'integer', autoIncrement:true, primaryKey:true},
  	name:{type:'string', required:true},
  	alias:{type:'string'},
  	description:{type:'string'},
  	databases_id:{type:'integer', required:true, model:'Databases'},
  	objects:{
  		collection:'Objects',
  		via:'schemas_id'
  	}
  },
  	beforeCreate: function(values, next) {
    // validacion de las restricciones (foreignKey)
    Databases.findOne(values.databases_id, function(err, value){
      if (err || !value){
        return next({"error": "databases_id does not exist."});
      }
      return next();
    });
  }
};

