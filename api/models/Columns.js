/**
* Columns.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName:'tbl_columns',
  attributes: {
  	id:{type:'integer', autoIncrement:true, primaryKey:true},
  	name:{type:'string', required:true},
  	alias:{type:'string'},
  	description:{type:'string'},
  	objects_id:{type:'integer', required:true, model:'Objects'},
  	if_external:{type:'boolean'},
  	external_objects_id:{type:'integer', model:'Objects'}
  },
  	beforeCreate: function(values, next) {
    // validacion de las restricciones (foreignKey)
    Objects.findOne(values.objects_id, function(err, value){
      if (err || !value){
        return next({"error": "objects_id does not exist."});
      }
    });

    Objects.findOne(values.external_objects_id, function(err, value){
      if (err || !value){
        return next({"error": "external_objects_id does not exist."});
      }
      return next();
    });


  }
  
};

