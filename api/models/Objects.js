/**
* Objects.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName:'tbl_objects',
  attributes: {
  	id:{type:'integer', autoIncrement:true, primaryKey:true},
  	name:{type:'string', required:true},
  	description:{type:'string'},
  	schemas_id:{type:'integer', model:'Schemas'},
  	columns:{
  		collection:'Columns',
  		via:'objects_id'
  	}
  },
  	beforeCreate: function(values, next) {
    // validacion de las restricciones (foreignKey)
    Schemas.findOne(values.schemas_id, function(err, value){
      if (err || !value){
        return next({"error": "schema_id does not exist."});
      }
      return next();
    });
  }
};

