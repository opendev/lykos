/**
* Databases.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName:'tbl_databases',
  attributes: {

  	id:{type:'integer', autoIncrement:true, primaryKey:true},
  	name:{type:'string', required:true},
  	alias:{type:'string'},
  	description:{type:'string'},
  	connection_id:{type:'integer', required:true, model:'Connection'},
  	schemas: {
  		collection:'Schemas',
  		via:'databases_id'
  	},

  },
  	beforeCreate: function(values, next) {
    // validacion de las restricciones (foreignKey)
    Connection.findOne(values.connection_id, function(err, value){
      if (err || !value){
        return next({"error": "connection_id does not exist."});
      }
      return next();
    });
  }
};

