/**
* Columns_objects.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName:'tbl_columns_objects',
  attributes: {
  	id:{type:'integer', autoIncrement:true, primaryKey:true},
  	columns_id:{type:'integer', required:true, model:'Columns'},
  	objects_id:{type:'integer', required:true, model:'Objects'}
  }
};

